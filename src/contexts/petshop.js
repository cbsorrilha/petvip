import {createContext} from 'react'

export default createContext({ petshop: null, setPetshop: () => {} })