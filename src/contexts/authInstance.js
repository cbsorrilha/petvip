import {createContext} from 'react'

export default createContext({ authInstance: null, user: null })