import React from 'react'
import { buttonStyles } from './main-button.module.css';

export default function MainButton({ children,  isLoading = false, type = "submit" }) {
  return (
    <button
      className={`button is-medium is-fullwidth is-primary is-rounded ${ isLoading ? 'is-loading' : '' } ${buttonStyles}`}
      type={ type }>
      {children}
    </button>
  )
}
