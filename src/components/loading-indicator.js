import React from "react";
import Page from "./page";

export default function LoadingIndicator() {
  return (
    <Page data-testid="loading">
      <h1 className="title has-text-centered">Carregando....</h1>
    </Page>
  );
}
