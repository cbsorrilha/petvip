import React from 'react'
import { title, iconStyles } from './title.module.css';

export default function Title({ children, icon = null}) {
    return (
        <h2 className={`title ${title}`}>
            { children }
            { icon !== null &&  <i data-testid="icon" className={`fas ${icon} ${iconStyles}`}></i>}
        </h2>
    )
}
