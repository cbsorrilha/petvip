import React, { useState } from "react";
import {
  addButton,
  addButtonActive,
  addArea,
  addAreaActive,
  addLinks,
  addLink,
  addIcons,
  addWrapper
} from "./add-button.module.css";
import { Link } from "@reach/router";

export default function AddButton() {
  const [active, setActive] = useState(false);

  const activate = () => {
    setActive(!active);
  };

  return (
    <div className={`${addWrapper}`}>
      <div className={`${addArea} ${active ? addAreaActive : ""}`}>
        {active && (
          <div className={addLinks}>
            <Link data-testid="add-agendamentos" className={`${addLink}`} onClick={activate} to="agendamentos/adicionar">
              <i className={`far fa-calendar-alt ${addIcons}`}></i>
            </Link>
            <Link data-testid="add-pets" className={`${addLink}`} onClick={activate} to="clientes/pets/adicionar">
              <i className={`fas fa-paw ${addIcons}`}></i>
            </Link>
            <Link data-testid="add-clientes" className={`${addLink}`} onClick={activate} to="clientes/adicionar">
              <i className={`fas fa-user ${addIcons}`}></i>
            </Link>
          </div>
        )}
      </div>
      <button
        data-testid="add-button"
        onClick={activate}
        className={`${addButton} ${
          active ? addButtonActive : ""
        } button is-link`}
      >
        +
      </button>
    </div>
  );
}
