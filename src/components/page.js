import React from 'react'
import { page, start } from './page.module.css'

export default function Page({ children, isStart = false, className, ...props }) {
    return (
        <div className={`${page} ${isStart ? start : ''} ${className}`} {...props}>
            { children }
        </div>
    )
}
