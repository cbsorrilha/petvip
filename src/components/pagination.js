import React from 'react';
import range from 'lodash/range';
import cc from 'classcat';
import { paginationStyles, buttonStyles } from './pagination.module.css';


const PaginationButtons = ({
  gotoPage,
  pageIndex,
  pageCount,
  pageArr,
}) => {
  if (pageCount <= 6) {
    return (
      <>
        {pageArr.map((p) => (
          <button
            key={p}
            type="button"
            data-testid="pagination-button"
            className={cc(['button', buttonStyles, 'is-size-7', { 'is-light': p !== pageIndex }, { 'is-primary': p === pageIndex }])}
            onClick={() => gotoPage(p)}
          >{p + 1}
          </button>
        ))}
      </>
    );
  }

  // Maior que a segunda página e menor que a penúltima página.
  if (pageIndex > 1 && pageIndex < pageCount - 2) {
    return (
      <>
        <button
          key={pageArr[0]}
          type="button"
          data-testid="pagination-button"
          className={cc(['button', buttonStyles, 'is-size-7', { 'is-light': pageArr[0] !== pageIndex }, { 'is-primary': pageArr[0] === pageIndex }])}
          onClick={() => gotoPage(pageArr[0])}
        >{pageArr[0] + 1}
        </button>
        <button  data-testid="remainder" type="button" className={cc(['button', buttonStyles, 'is-size-7', 'is-light'])}>...</button>
        {
          pageArr.slice(pageIndex - 1, pageIndex + 2).map((p) => (
            <button
              key={p}
              type="button"
              data-testid="pagination-button"
              className={cc(['button', buttonStyles, 'is-size-7', { 'is-light': p !== pageIndex }, { 'is-primary': p === pageIndex }])}
              onClick={() => gotoPage(p)}
            >
              {p + 1}
            </button>
          ))
        }
        <button data-testid="remainder" type="button" className={cc(['button', buttonStyles, 'is-size-7', 'is-light'])}>...</button>
        <button
          key={pageArr[pageCount]}
          type="button"
          data-testid="pagination-button"
          className={cc(['button', buttonStyles, 'is-size-7', { 'is-light': pageCount - 1 !== pageIndex }, { 'is-primary': pageCount - 1 === pageIndex }])}
          onClick={() => gotoPage(pageCount - 1)}
        >{pageCount}
        </button>
      </>
    );
  }
  return (
    <>
      {pageArr.slice(0, 3).map((p) => (
        <button
          key={p}
          type="button"
          data-testid="pagination-button"
          className={cc(['button', buttonStyles, 'is-size-7', { 'is-light': p !== pageIndex }, { 'is-primary': p === pageIndex }])}
          onClick={() => gotoPage(p)}
        >{p + 1}
        </button>
      ))}
      <button data-testid="remainder" type="button" className={cc(['button', buttonStyles, 'is-light', 'is-size-7'])}>...</button>
      {pageArr.slice(pageCount - 3, pageCount).map((p) => (
        <button
          key={p}
          type="button"
          data-testid="pagination-button"
          className={cc(['button', buttonStyles, 'is-size-7', { 'is-light': p !== pageIndex }, { 'is-primary': p === pageIndex }])}
          onClick={() => gotoPage(p)}
        >{p + 1}
        </button>
      ))}
    </>
  );
};

const Pagination = ({
  canPreviousPage,
  canNextPage,
  gotoPage,
  nextPage,
  previousPage,
  pageIndex,
  pageCount,
}) => {
  const pageArr = range(0, pageCount);

  return (
    <div data-testid="pagination" className={`pagination buttons has-addons is-centered ${paginationStyles}`}>
      <button
        type="button"
        data-testid="pagination-prev"
        className="button is-light is-rounded is-size-7"
        onClick={() => previousPage()}
        disabled={!canPreviousPage}
      >
        <i className="fa fa-chevron-left" />
      </button>
      <PaginationButtons
        gotoPage={gotoPage}
        pageIndex={pageIndex}
        pageCount={pageCount}
        pageArr={pageArr}
      />
      <button
        type="button"
        data-testid="pagination-next"
        className="button is-light is-rounded is-size-7"
        onClick={() => nextPage()}
        disabled={!canNextPage}
      >
        <i className="fa fa-chevron-right" />
      </button>
    </div>
  );
};

export default Pagination