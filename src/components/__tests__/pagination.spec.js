import React from "react"
import { act, render } from "@testing-library/react"
import Pagination from "../pagination"

describe("Test the pagination", () => {
  const state = {}

  beforeEach(() => {
      state.canPreviousPage = jest.fn()
      state.canNextPage = jest.fn()
      state.gotoPage = jest.fn()
      state.nextPage = jest.fn()
      state.previousPage = jest.fn()
      state.pageIndex = 0
      state.pageCount = 10
  })

  it("should render the pagination", async () => {
    await act(async () => {
      const { getByTestId } = render(<Pagination {...state}  />)
      const element = getByTestId('pagination')
      expect(element).toBeInTheDocument()
      const prevBtn = getByTestId('pagination-prev')
      expect(prevBtn).toBeInTheDocument()
      const nextBtn = getByTestId('pagination-next')
      expect(nextBtn).toBeInTheDocument()
    })
  })

  it("should render the pagination with 6 or less pages", async () => {
    await act(async () => {
      state.pageCount = 6
      const { queryAllByTestId, queryByTestId } = render(<Pagination {...state}  />)
      const elements = queryAllByTestId('pagination-button')
      expect(elements.length).toBe(6)
      const remainderBtn = queryByTestId('remainder')
      expect(remainderBtn).not.toBeInTheDocument()
    })
  })

  it("should render with 6 or more and the selected page is one of the first two", async () => {
    await act(async () => {
      state.pageCount = 10
      const { queryAllByTestId, getByText } = render(<Pagination {...state}  />)
      const elements = queryAllByTestId('pagination-button')
      expect(elements.length).toBe(6) //only pick the visible elements
      const remainderBtns = queryAllByTestId('remainder')
      expect(remainderBtns.length).toBe(1)
      const firstBtn = getByText('1')
      expect(firstBtn).toBeInTheDocument()
      const lastBtn = getByText('10')
      expect(lastBtn).toBeInTheDocument()
    })
  })

  it("should render with 6 or more and the selected page is after first two", async () => {
    await act(async () => {
      state.pageCount = 10
      state.pageIndex = 4
      const { queryAllByTestId, getByText, queryByText } = render(<Pagination {...state}  />)
      const elements = queryAllByTestId('pagination-button')
      expect(elements.length).toBe(5) //only pick the visible elements
      const remainderBtns = queryAllByTestId('remainder')
      expect(remainderBtns.length).toBe(2)
      const firstBtn = getByText('1')
      expect(firstBtn).toBeInTheDocument()
      const lastBtn = getByText('10')
      expect(lastBtn).toBeInTheDocument()
      const secondBtn = queryByText('2')
      expect(secondBtn).not.toBeInTheDocument()
    })
  })
})
