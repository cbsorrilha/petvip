import React from "react";
import { act, render } from "@testing-library/react";
import Navbar from "../navbar";

describe("Test the navbar", () => {
  it("should render the link to agendamentos", async () => {
    await act(async () => {
      const { getByTestId } = render(<Navbar />);
      const element = getByTestId('navigation-agendamentos');
      expect(element).toBeInTheDocument();
    });
  });

  it("should render the link to clientes", async () => {
    await act(async () => {
      const { getByTestId } = render(<Navbar />);
      const element = getByTestId('navigation-clientes');
      expect(element).toBeInTheDocument();
    });
  });

  it("should render the link to clubinhos", async () => {
    await act(async () => {
      const { getByTestId } = render(<Navbar />);
      const element = getByTestId('navigation-clubinhos');
      expect(element).toBeInTheDocument();
    });
  });

  it("should render the link to configuracoes", async () => {
    await act(async () => {
      const { getByTestId } = render(<Navbar />);
      const element = getByTestId('navigation-configuracoes');
      expect(element).toBeInTheDocument();
    });
  });

  it("should render the add button", async () => {
    await act(async () => {
      const { getByTestId } = render(<Navbar />);
      const element = getByTestId('add-button');
      expect(element).toBeInTheDocument();
    });
  });
});
