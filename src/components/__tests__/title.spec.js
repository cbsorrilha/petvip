import React from "react";
import { render } from "@testing-library/react";
import Title from "../title";

describe("Test the title component", () => {
  it("should render the title text", () => {
    const { getByText } = render(<Title>bazzinga</Title>);
    const element = getByText(/bazzinga/);
    expect(element).toBeInTheDocument();
  });

  it("should render the title with icon", () => {
    const { getByTestId, getByText } = render(<Title icon="fa-user">bazzinga</Title>);
    const icon = getByTestId('icon');
    const element = getByText(/bazzinga/);

    expect(element).toBeInTheDocument();
    expect(icon).toBeInTheDocument();
  });

});
