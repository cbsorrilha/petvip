import React from "react";
import { act, render, fireEvent, waitForElement } from "@testing-library/react";
import AddButton from "../add-button";

describe("Test the add button", () => {
  it("should render the add button", async () => {
    await act(async () => {
      const { getByTestId } = render(<AddButton />);
      const element = getByTestId('add-button');
      expect(element).toBeInTheDocument();
    });
  });

  it("should render the add links after click the add button", async () => {
    await act(async () => {
      const { getByTestId } = render(<AddButton />);
      const btn = getByTestId('add-button');
      fireEvent.click(btn)
      const element1 = await waitForElement(() => getByTestId('add-agendamentos'))
      const element2 = await waitForElement(() => getByTestId('add-agendamentos'))
      const element3 = await waitForElement(() => getByTestId('add-agendamentos'))

      expect(element1).toBeInTheDocument();
      expect(element2).toBeInTheDocument();
      expect(element3).toBeInTheDocument();
    });
  });

});
