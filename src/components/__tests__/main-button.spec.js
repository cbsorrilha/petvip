import React from "react";
import { render } from "@testing-library/react";
import MainButton from "../main-button";

describe("Test the main button component", () => {
  it("should render the button text", () => {
    const { getByText } = render(<MainButton>bazzinga</MainButton>);
    const element = getByText(/bazzinga/);
    expect(element).toBeInTheDocument();
  });

  it("should render the button text", async () => {
    const { getByText } = render(<MainButton isLoading={true}>bazzinga</MainButton>);
    const element = getByText(/bazzinga/);
    expect(element).toHaveClass('is-loading')
  });
});
