import React, { useState, useEffect } from "react";
import {
  navbar,
  navbarContent,
  navbarIcons,
  navbarLink,
} from "./navbar.module.css";
import { Link } from "@reach/router";
import AddButton from "./add-button";

export default function NavBar() {
  const [showNavbar, setShowNavbar] = useState(true);

  useEffect(() => {
    if ("visualViewport" in window) {
      const clientHeight = document.scrollingElement.clientHeight;
      window.visualViewport.addEventListener("resize", function (event) {
        if (event.target.height + 30 < clientHeight) {
          setShowNavbar(false);
        } else {
          setShowNavbar(true);
        }
      });
    }
  }, []);

  if (!showNavbar) {
    return null;
  }

  return (
    <nav className={navbar}>
      <div className={navbarContent}>
        <Link
          className={`${navbarLink}`}
          data-testid="navigation-agendamentos"
          to="/app"
        >
          <i className={`far fa-calendar-alt ${navbarIcons}`}></i>
          <span>agendamentos</span>
        </Link>
        <Link
          className={`${navbarLink}`}
          data-testid="navigation-clientes"
          to="clientes"
        >
          <i className={`fas fa-users ${navbarIcons}`}></i>
          <span>clientes</span>
        </Link>
        <div>
          <AddButton />
        </div>
        <Link
          className={`${navbarLink}`}
          data-testid="navigation-clubinhos"
          to="clubinhos"
        >
          <i className={`fas fa-star ${navbarIcons}`}></i>
          <span>clubinhos</span>
        </Link>
        <Link
          className={`${navbarLink}`}
          data-testid="navigation-configuracoes"
          to="configuracoes"
        >
          <i className={`fas fa-wrench  ${navbarIcons}`}></i>
          <span>configurações</span>
        </Link>
      </div>
    </nav>
  );
}
