import { cellphoneMask } from '../form'


describe('Testing the form utility module', () => {
  describe('Testing the cellphoneMask utility', () => {
    it('should receive a 8 digits telephone number and return it masked', () => {
      const returned = cellphoneMask('2126560022');
      expect(returned).toEqual(["(", /[1-9]/, /[1-9]/, ")", " ", /\d/, /\d/, /\d/, /\d/, "-", /\d/, /\d/, /\d/, /\d/])
    })
  })

})
