import { renderHook, act } from '@testing-library/react-hooks';
import {usePagination} from '../pagination';

describe('Test the usePagination', () => {
  const state = {}
  beforeAll(() => {
    state.limit = 4
    state.total = 45
  })

  it('should return the initial pagination state', () => {
    const { result } = renderHook(() => usePagination(state.limit, state.total))
    const {
      offset,
      canNext,
      canPrevious,
      page,
      limit,
    } = result.current[0]
    expect(offset).toBe(0)
    expect(canNext).toBe(true)
    expect(canPrevious).toBe(false)
    expect(page).toBe(0)
    expect(limit).toBe(4)
    const {
      onPrevious,
      onNext,
      onPageSelect,
    } = result.current[1]
    expect(typeof onPrevious).toBe('function')
    expect(typeof onNext).toBe('function')
    expect(typeof onPageSelect).toBe('function')
  })
  it('should advance a page when onNext is called', () => {
    const { result } = renderHook(() => usePagination(state.limit, state.total))
    act(() => {
      result.current[1].onNext()
    })

    const {offset,
      canNext,
      canPrevious,
      page,} = result.current[0]
    expect(offset).toBe(4)
    expect(canNext).toBe(true)
    expect(canPrevious).toBe(true)
    expect(page).toBe(1)
  })
  it('should advance a page when onPrev is called', () => {
    const { result } = renderHook(() => usePagination(state.limit, state.total))
    act(() => {
      result.current[1].onNext()
    })
    act(()=> {
      result.current[1].onPrevious()
    })

    const {offset,
      canNext,
      canPrevious,
      page,} = result.current[0]
    expect(offset).toBe(0)
    expect(canNext).toBe(true)
    expect(canPrevious).toBe(false)
    expect(page).toBe(0)
  })

  it('should go to the selected page when onPageSelected', () => {
    const { result } = renderHook(() => usePagination(state.limit, state.total))
    
    act(() => {
      result.current[1].onPageSelect(3)
    })

    const {offset,
      canNext,
      canPrevious,
      page,} = result.current[0]
    expect(offset).toBe(12)
    expect(canNext).toBe(true)
    expect(canPrevious).toBe(true)
    expect(page).toBe(3)
  })
})

