import React from 'react';
import { render } from '@testing-library/react';
import withAuthCheck from '../auth-check';
import * as firebase from 'firebase/app';

describe('Test the withAuthCheck', () => {
  beforeAll(() => {
    firebase.auth = jest.fn().mockReturnValue({
      currentUser: true,
      signOut: () => { return true },
      onAuthStateChanged: func => {
        func(true)
        return () => {}
      },
    })
  })

  it('should render the withAuthCheck HOC in final state', () => {
    const Component = withAuthCheck(() => <h1>Teste</h1>)
    const { getByText } = render(<Component />)
    const titleElement = getByText(/Teste/i)
    expect(titleElement).toBeInTheDocument()
  })
})

