
import {useState, useMemo} from 'react'

export const usePagination = (limit, total) => {
  const pageCount = Math.ceil(total / limit)
  const [state, setState] = useState({
    offset: 0,
    canNext: true,
    canPrevious: false,
    page: 0,
    limit
  })

  const onPrevious = () => {
    const newOffset = state.offset === 0 ? 0 : state.offset - limit
    setState({
      ...state,
      offset: newOffset,
      canNext: newOffset < total - limit,
      canPrevious: newOffset > 0,
      page: state.page - 1,
    })
  }

  const onPageSelect = (newPageIndex) => {
    const cannnotPreviousPage = newPageIndex < 0
    const cannotNextPage = newPageIndex > pageCount - 1

    if (cannnotPreviousPage || cannotNextPage) {
      return
    }

    setState({
      ...state,
      offset: newPageIndex * limit,
      page: newPageIndex,
      canNext: newPageIndex !== pageCount - 1,
      canPrevious: newPageIndex !== 0,
    })
  }

  const onNext = () => {
    const newOffset = state.page === (pageCount - 1) ? state.offset : state.offset + limit
    setState({
      ...state,
      offset: newOffset,
      canNext: state.page < pageCount - 1,
      canPrevious: newOffset > 0,
      page: state.page + 1,
    })
  }

  const actions = {
    onPrevious,
    onNext,
    onPageSelect,
  }
  /* eslint-disable-next-line react-hooks/exhaustive-deps */
  return useMemo(() => [{...state, pageCount}, actions], [total, limit, state.page])
}