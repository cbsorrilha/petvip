import { useState } from 'react';

export const makeFieldUpdater = (updater, state) => ({ target }) => {
  updater({ ...state, [target.name]: target.value });
};

export const useForm = (initialState) => {
  const [state, setState] = useState(initialState);
  return [state, makeFieldUpdater(setState, state), setState];
};

export const cellphoneMask = (userInput) => {
  let numbers = userInput.match(/\d/g);
  let numberLength = 0;
  if (numbers) {
    numberLength = numbers.join('').length;
  }

  if (numberLength > 10) {
    return [
      '(',
      /[1-9]/,
      /[1-9]/,
      ')',
      ' ',
      /\d/,
      /\d/,
      /\d/,
      /\d/,
      /\d/,
      '-',
      /\d/,
      /\d/,
      /\d/,
      /\d/,
    ];
  } else {
    return [
      '(',
      /[1-9]/,
      /[1-9]/,
      ')',
      ' ',
      /\d/,
      /\d/,
      /\d/,
      /\d/,
      '-',
      /\d/,
      /\d/,
      /\d/,
      /\d/,
    ];
  }
};
