import React, { useState, useEffect } from "react";
import { Redirect } from "@reach/router";
import { auth } from "firebase/app";
import LoadingIndicator from "../components/loading-indicator";

export default function withAuthCheck(WrappedComponent) {
  return function(props) {
    const authInstance = auth();
    const [isLoading, changeLoading] = useState(true);
    const [authUser, changeAuthUser] = useState(authInstance.currentUser);

    useEffect(() => {
      authInstance.onAuthStateChanged(function(user) {
        if (user) {
          changeAuthUser(authInstance.currentUser);
          changeLoading(false);
        } else {
          changeAuthUser(null);
          changeLoading(false);
        }
      });
    }, [authInstance]);

    if (!isLoading && authUser) {
      return (
        <WrappedComponent
          {...props}
          user={authUser}
          authInstance={authInstance}
        />
      );
    }

    if (!isLoading && !authUser) {
      return <Redirect noThrow to="/login/" />;
    }

    return <LoadingIndicator />;
  };
}
