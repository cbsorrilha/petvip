import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/analytics";
import "firebase/firestore";

const {
  REACT_APP_FIREBASE_API_KEY,
  REACT_APP_FIREBASE_AUTH_DOMAIN,
  REACT_APP_FIREBASE_DATABASE_URL,
  REACT_APP_FIREBASE_PROJECT_ID,
  REACT_APP_FIREBASE_STORAGE_BUCKET,
  REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
  REACT_APP_FIREBASE_APP_ID,
  REACT_APP_FIREBASE_MEASUREMENT_ID,
  NODE_ENV
} = process.env;

export async function initializeFirebase() {
  try {
    switch (NODE_ENV) {
      case "test":
        return;
      case "development":
        const firebaseConfig = {
          apiKey: REACT_APP_FIREBASE_API_KEY,
          authDomain: REACT_APP_FIREBASE_AUTH_DOMAIN,
          databaseURL: REACT_APP_FIREBASE_DATABASE_URL,
          projectId: REACT_APP_FIREBASE_PROJECT_ID,
          storageBucket: REACT_APP_FIREBASE_STORAGE_BUCKET,
          messagingSenderId: REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
          appId: REACT_APP_FIREBASE_APP_ID,
          measurementId: REACT_APP_FIREBASE_MEASUREMENT_ID
        };
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);
        return firebase;
      default:
        const response = await fetch("/__/firebase/init.json");
        firebase.initializeApp(await response.json());
        firebase.analytics();
        return firebase;
    }
  } catch (error) {
    console.error("Firebase not initialized", error);
  }
}
