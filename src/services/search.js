import Axios from 'axios';
const SEARCH_API = process.env.REACT_APP_SEARCH_API;

const mountQuery = (params) =>
  Object.keys(params).reduce((stack, next) => {
    return stack.indexOf('?') !== -1
      ? `${stack}&${next}=${params[next]}`
      : `?${next}=${params[next]}`;
  }, '');

export const searchCustomerByQuery = (petshopId) => (params) => {
  const baseUrl = `${SEARCH_API}${petshopId.toLowerCase()}/customer/search`;
  return Axios.get(`${baseUrl}${mountQuery(params)}`).then((resp) => ({
    ...resp.data,
    data: resp.data.data.map((obj) => obj._source),
  }));
};

export const addCustomerToSearchApi = (petshopId) => (customer) =>
  Axios.post(`${SEARCH_API}${petshopId.toLowerCase()}/customer/add`, customer);

export const addPetToCustomerInSearchApi = (petshopId) => (pet) =>
  Axios.patch(`${SEARCH_API}${petshopId.toLowerCase()}/customer/pet/add`, pet);

export const editCustomerToSearchApi = (petshopId) => (customerId, customer) =>
  Axios.put(
    `${SEARCH_API}${petshopId.toLowerCase()}/customer/${customerId}`,
    customer,
  );
