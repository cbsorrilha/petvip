import _ from "lodash";
import { createCustomer } from "./customer";
import { createPet } from "./pet";
import faker from "faker";

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export async function generate100Customers(petshop) {
  const mockCreateCustomer = createCustomer(petshop);
  const mockCreatePet = createPet(petshop);
  const creating = _.range(10).map(async () => {
    const customerId = await mockCreateCustomer({
      name: faker.name.findName(),
      address: faker.address.streetAddress(),
      telephone: faker.phone.phoneNumber(),
      email: faker.internet.email(),
    });
    console.log("customer", customerId);
    const pet = await mockCreatePet({
      name: faker.hacker.noun(),
      race: faker.random.arrayElement([
        "Pinscher",
        "Rotwheiler",
        "Pitbull",
        "Pug",
        "Shitzu",
        "Shitu",
        "Poodle",
        "Pudo",
        "Shiba Inu",
      ]),
      observation: faker.lorem.sentence(),
      ownerId: customerId,
    });
    console.log("pet", pet);
    await sleep(5000);
  });
  return Promise.all(creating);
}

// address
// "97676 Murazik Field"
// email
// "Aaliyah36@gmail.com"
// name
// "George Daugherty"
// petshopId
// /petshops/oEWSXYs96m0fDBnvUbhM
// telephone
// "598087133541501"
