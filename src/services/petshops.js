import { firestore } from "firebase/app";

export const getPetshop = async (user) => {
  try {
    const db = firestore();
    const querySnapshot = await db
      .collection("petshops")
      .where("ownerId", "==", user.uid)
      .get();
    let data = null;
    querySnapshot.forEach((doc) => {
      data = {
        ...doc.data(),
        id: doc.id,
        ref: db.doc(`/petshops/${doc.id}`),
      };
    });
    return data;
  } catch (error) {
    return [];
  }
};

export const createPetshop = async (petshop) => {
  try {
    const db = firestore();
    const ref = await db.collection("petshops").add(petshop);
    const doc = await ref.get();
    return {
      ...doc.data(),
      id: doc.id,
      ref,
    };
  } catch (error) {
    console.error(error);
    throw new Error(error);
  }
};
