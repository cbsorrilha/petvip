import Axios from 'axios'

export default {
    get: cep => {
        return Axios.get(`//viacep.com.br/ws/${cep}/json/`)
    }
}