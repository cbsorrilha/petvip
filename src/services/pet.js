import { firestore } from "firebase/app"
import { addPetToCustomerInSearchApi } from '../services/search'

export const createPet = (petshop) => async pet => {
  try {
    const db = firestore()
    const docRef = await db.collection('pets').add({
        ...pet,
        petshopId: petshop.ref
    })
    const created = {
      ...pet,
      id: docRef.id,
      petshopId: petshop.ref
    }
    await addPetToCustomerInSearchApi(petshop.id)({
      ...pet,
      id: docRef.id,
    })

    return created
  } catch (error) {
    console.error(error)
    throw new Error(error)
  }
}

export const getPets = async customerId => {
  try {
    const db = firestore()
    const querySnapshot = await db.collection('pets').where("ownerId", "==", customerId).get()
    let data = []
    querySnapshot.forEach((doc) => {
        data.push({
            ...doc.data(),
            id: doc.id,
            ref: db.doc(`/pets/${doc.id}`)
        })
    })
    return data
  } catch (error) {
    return []
  }
}