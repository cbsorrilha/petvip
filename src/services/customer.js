import { firestore } from "firebase/app";
import {
  addCustomerToSearchApi,
  searchCustomerByQuery,
  editCustomerToSearchApi,
} from "./search";

export const createCustomer = (petshop) => async (customer) => {
  try {
    const db = firestore();
    const docRef = await db.collection("customers").add({
      ...customer,
      telephone: customer.telephone.replace(/\D/g, ""),
      petshopId: petshop.ref,
    });

    await addCustomerToSearchApi(petshop.id)({
      ...customer,
      telephone: customer.telephone.replace(/\D/g, ""),
      id: docRef.id,
    });

    return docRef.id;
  } catch (error) {
    console.error("error", error);
    if (error.message) {
      console.error("Error message", error.message);
    }
    throw new Error(error);
  }
};

export const getCustomers = (petshop) => async (pagination) => {
  try {
    const { limit = "4", offset = 0 } = pagination;
    const result = await searchCustomerByQuery(petshop.id)({ limit, offset });
    return result;
  } catch (error) {
    console.error(error);
    return [];
  }
};

export const getCustomer = async (customerId) => {
  try {
    const db = firestore();
    const doc = await db.collection("customers").doc(customerId).get();
    if (!doc.exists) {
      throw new Error("Cliente não existe!");
    }
    return {
      ...doc.data(),
      id: doc.id,
    };
  } catch (error) {
    console.error(error);
    throw error;
  }
};

export const updateCustomer = (petshop) => async (customerId, customer) => {
  try {
    const db = firestore();

    const oldCustomer = await db.collection("customers").doc(customerId);

    await oldCustomer.update({
      ...customer,
      telephone: customer.telephone.replace(/\D/g, ""),
      petshopId: petshop.ref,
    });

    delete customer.petshopId;
    console.log("customer", customer);

    await editCustomerToSearchApi(petshop.id)(customerId, {
      ...customer,
      telephone: customer.telephone.replace(/\D/g, ""),
      id: customerId,
    });

    return customerId;
  } catch (error) {
    console.error("error", error);
    throw new Error(error);
  }
};
