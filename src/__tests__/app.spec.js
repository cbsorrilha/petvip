import React from "react";
import { act, waitForElementToBeRemoved, render } from "@testing-library/react";
import App from "../app";

describe("Test the petvip app", () => {
  it("should render the loading page", async () => {
    await act(async () => {
      const { getByTestId } = render(<App />);
      const loadingElement = getByTestId("loading");
      expect(loadingElement).toBeInTheDocument();
    });
  });

  it("should render the title", async () => {
    await act(async () => {
      const { getByText, getByTestId } = render(<App />);
      await waitForElementToBeRemoved(() => getByTestId("loading"));
      const titleElement = getByText(/Petvip - App/i);
      expect(titleElement).toBeInTheDocument();
    });
  });
});
