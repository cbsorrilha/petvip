import React, { useState, useEffect } from "react";
import { Router } from "@reach/router";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import Site from "./pages/site";
import Login from "./pages/login";
import Internal from "./pages/internal";
import LoadingPage from "./pages/loading-page";
import "./app.css";
import { initializeFirebase } from "./utils/firebase";

toast.configure();

function App() {
  const [loadingFirebase, setLoadingFirebase] = useState(true);

  async function loadFirebase() {
    await initializeFirebase();
    setLoadingFirebase(false);
  }

  useEffect(() => {
    loadFirebase();
  }, []);

  if (loadingFirebase) {
    return <LoadingPage />;
  }

  return (
    <Router>
      <Site path="/" />
      <Login path="login/" />
      <Internal path="app/*" />
    </Router>
  );
}

export default App;
