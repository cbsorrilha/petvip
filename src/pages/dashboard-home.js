import React, { useContext, useState } from "react";
import Page from "../components/page";
import { useNavigate, Redirect } from "@reach/router";
import AuthInstanceContext from "../contexts/authInstance";
import PetshopContext from "../contexts/petshop";
import { generate100Customers } from "../services/mock";

export default function DashboardHome() {
  const { user, authInstance } = useContext(AuthInstanceContext);
  const { petshop } = useContext(PetshopContext);
  const [creating, setCreating] = useState(false);

  const navigate = useNavigate();
  const signOut = async () => {
    await authInstance.signOut();
    navigate("../");
  };

  const generate = async () => {
    setCreating(true);
    await generate100Customers(petshop);
    setCreating(false);
  };

  if (!user) {
    return <Redirect to="/login" noThrow />;
  }

  if (!petshop) {
    return <Redirect to="register" noThrow />;
  }

  return (
    <Page>
      <h1 className="title has-text-centered">Olá {user.displayName}</h1>
      <h2 className="subtitle  has-text-centered">
        dono do petshop {petshop.name}
      </h2>
      {process.env.NODE_ENV === "development" && (
        <button
          disabled={creating}
          onClick={generate}
          className="button is-medium is-fullwidth is-primary is-rounded"
        >
          {creating ? "Gerando...." : "Gerar 100 mocks"}
        </button>
      )}
      <button
        onClick={signOut}
        className="button is-medium is-fullwidth is-primary is-rounded"
      >
        <span className="has-text-weight-bold">Sair</span>
      </button>
    </Page>
  );
}
