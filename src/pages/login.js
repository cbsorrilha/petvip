import React, { useState } from "react";
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth";
import { auth } from "firebase/app";
import { pendingLoginPage, loginContent } from "./login.module.css";
import Page from "../components/page";

export default function Login() {
  const [pending, setPending] = useState(false);
  const uiConfig = {
    signInSuccessUrl: "/app",
    signInOptions: [
      auth.EmailAuthProvider.PROVIDER_ID,
      auth.GoogleAuthProvider.PROVIDER_ID,
    ],
  };

  auth.languageCode = "pt-BR";

  const checkPendingRedirect = (ui) => {
    if (ui.isPendingRedirect()) {
      setPending(true);
    }
  };

  const pendingClass = pending ? pendingLoginPage : "";

  return (
    <Page className={`${pendingClass}`}>
      <div className={`${loginContent}`}>
        {!pending && (
          <h1 className={`title has-text-centered`}>
            Olá, para começar faça login:
          </h1>
        )}
        <StyledFirebaseAuth
          uiCallback={checkPendingRedirect}
          uiConfig={uiConfig}
          firebaseAuth={auth()}
        />
      </div>
    </Page>
  );
}
