import React from "react";
import {
  act,
  waitForElementToBeRemoved,
  waitForElement,
  queryByText,
  fireEvent,
} from "@testing-library/react";
import CreatePetPage from "../create";
import PetshopContext from "../../../contexts/petshop";
import { renderWithRouter } from "../../../test-utils";
import { createPet, getPets } from "../../../services/pet";
import { getCustomer } from "../../../services/customer";

function renderCreatePetPage() {
  return renderWithRouter(
    <PetshopContext.Provider value={{ petshop: { ownerId: "1" } }}>
      <CreatePetPage />
    </PetshopContext.Provider>
  );
}

jest.mock("../../../services/pet");
jest.mock("../../../services/customer");

describe("Test the create pet page", () => {
  describe("Test the create pet page without previous pets", () => {
    beforeEach(() => {
      getCustomer.mockResolvedValue({ name: "jooj" });
      getPets.mockResolvedValue([]);
      createPet.mockReturnValue(() => Promise.resolve(true));
    });

    it("should render the page loader", async () => {
      await act(async () => {
        const { getByTestId } = renderCreatePetPage();
        const loadingElement = getByTestId("loading");
        expect(loadingElement).toBeInTheDocument();
      });
    });

    it("should render the page title", async () => {
      await act(async () => {
        const { getByText, getByTestId } = renderCreatePetPage();
        await waitForElementToBeRemoved(() => getByTestId("loading"));
        const title = getByText(/Novos Pets/i);
        expect(title).toBeInTheDocument();
      });
    });

    it("should render the customer name", async () => {
      await act(async () => {
        const { getByText, getByTestId } = renderCreatePetPage();
        await waitForElementToBeRemoved(() => getByTestId("loading"));
        const customerName = getByText(/jooj/i);
        expect(customerName).toBeInTheDocument();
      });
    });

    it("should not render the 'cancelar' or 'adicionar outro pet' buttons", async () => {
      await act(async () => {
        const { getByTestId, container } = renderCreatePetPage();
        await waitForElementToBeRemoved(() => getByTestId("loading"));
        const addNewPetBtn = queryByText(container, /Adicionar outro pet/i);
        expect(addNewPetBtn).not.toBeInTheDocument();
        const CancelBtn = queryByText(container, /Cancelar/i);
        expect(CancelBtn).not.toBeInTheDocument();
      });
    });

    it("should render the form", async () => {
      await act(async () => {
        const { getByLabelText, getByTestId } = renderCreatePetPage();
        await waitForElementToBeRemoved(() => getByTestId("loading"));
        const name = getByLabelText("Nome:");
        expect(name).toBeInTheDocument();
        const race = getByLabelText("Raça:");
        expect(race).toBeInTheDocument();
        const obs = getByLabelText("Observações:");
        expect(obs).toBeInTheDocument();
      });
    });

    it("should send the form", async () => {
      await act(async () => {
        const { getByLabelText, getByTestId } = renderCreatePetPage();
        await waitForElementToBeRemoved(() => getByTestId("loading"));

        const name = getByLabelText("Nome:");
        fireEvent.change(name, { target: { value: "23" } });
        const race = getByLabelText("Raça:");
        fireEvent.change(race, { target: { value: "23" } });
        const observations = getByLabelText("Observações:");
        fireEvent.change(observations, { target: { value: "23" } });
        const form = getByTestId("form");

        fireEvent.submit(form);

        expect(createPet).toHaveBeenCalled();
      });
    });

    describe("Test the create pet page with previous pets", () => {
      beforeEach(() => {
        getCustomer.mockResolvedValue({ name: "jooj" });
        getPets.mockResolvedValue([
          {
            name: "matola",
            race: "pinscher",
            observation: "n/a",
          },
        ]);
        createPet.mockReturnValue((props) => Promise.resolve(props));
      });

      it("should render 'adicionar outro pet' but not render the 'cancelar'", async () => {
        await act(async () => {
          const { getByTestId, container } = renderCreatePetPage();
          await waitForElementToBeRemoved(() => getByTestId("loading"));
          const addNewPetBtn = queryByText(container, /Adicionar outro pet/i);
          expect(addNewPetBtn).toBeInTheDocument();
          const CancelBtn = queryByText(container, /Cancelar/i);
          expect(CancelBtn).not.toBeInTheDocument();
        });
      });

      it("should render the pet data", async () => {
        await act(async () => {
          const { getByText, getByTestId } = renderCreatePetPage();
          await waitForElementToBeRemoved(() => getByTestId("loading"));
          const name = getByText(/matola/i);
          expect(name).toBeInTheDocument();
          const race = getByText(/pinscher/i);
          expect(race).toBeInTheDocument();
          const obs = getByText(/n\/a/i);
          expect(obs).toBeInTheDocument();
        });
      });

      it("should render the 'Confirmar' button", async () => {
        await act(async () => {
          const { getByText, getByTestId } = renderCreatePetPage();
          await waitForElementToBeRemoved(() => getByTestId("loading"));
          const btn = getByText(/Confirmar/i);
          expect(btn).toBeInTheDocument();
        });
      });

      it("should render the form when user click", async () => {
        await act(async () => {
          const {
            getByLabelText,
            getByTestId,
            container,
          } = renderCreatePetPage();
          await waitForElementToBeRemoved(() => getByTestId("loading"));

          const addNewPetBtn = queryByText(container, /Adicionar outro pet/i);

          fireEvent.click(addNewPetBtn);
          const name = await waitForElement(() => getByLabelText("Nome:"));
          const race = await waitForElement(() => getByLabelText("Raça:"));
          const obs = await waitForElement(() =>
            getByLabelText("Observações:")
          );

          expect(name).toBeInTheDocument();
          expect(race).toBeInTheDocument();
          expect(obs).toBeInTheDocument();
        });
      });

      it("should send the form after user clicks adicioanr", async () => {
        await act(async () => {
          const {
            getByLabelText,
            getByTestId,
            container,
          } = renderCreatePetPage();
          await waitForElementToBeRemoved(() => getByTestId("loading"));

          const addNewPetBtn = queryByText(container, /Adicionar outro pet/i);

          fireEvent.click(addNewPetBtn);

          const name = getByLabelText("Nome:");
          fireEvent.change(name, { target: { value: "23" } });
          const race = getByLabelText("Raça:");
          fireEvent.change(race, { target: { value: "23" } });
          const observations = getByLabelText("Observações:");
          fireEvent.change(observations, { target: { value: "23" } });
          const form = getByTestId("form");

          fireEvent.submit(form);

          expect(createPet).toHaveBeenCalled();
        });
      });

      it("should render the new pet on the list after form submission", async () => {
        await act(async () => {
          const {
            getByLabelText,
            getByTestId,
            getByText,
            container,
          } = renderCreatePetPage();
          await waitForElementToBeRemoved(() => getByTestId("loading"));

          const addNewPetBtn = queryByText(container, /Adicionar outro pet/i);

          fireEvent.click(addNewPetBtn);

          const name = getByLabelText("Nome:");
          fireEvent.change(name, { target: { value: "name2" } });
          const race = getByLabelText("Raça:");
          fireEvent.change(race, { target: { value: "race2" } });
          const observations = getByLabelText("Observações:");
          fireEvent.change(observations, { target: { value: "obs2" } });
          const form = getByTestId("form");

          fireEvent.submit(form);

          await waitForElementToBeRemoved(() => getByText(/Cadastrar/i));

          const name2 = getByText(/name2/i);
          expect(name2).toBeInTheDocument();
          const race2 = getByText(/race2/i);
          expect(race2).toBeInTheDocument();
          const obs2 = getByText(/obs2/i);
          expect(obs2).toBeInTheDocument();
        });
      });

      it("should test the cancelar button", async () => {
        await act(async () => {
          const { getByText, getByTestId, container } = renderCreatePetPage();
          await waitForElementToBeRemoved(() => getByTestId("loading"));

          const addNewPetBtn = queryByText(container, /Adicionar outro pet/i);

          fireEvent.click(addNewPetBtn);
          const cancel = await waitForElement(() =>
            queryByText(container, /Cancelar/i)
          );
          expect(cancel).toBeInTheDocument();
          fireEvent.click(cancel);
          const confirmBtn = getByText(/Confirmar/i);
          expect(confirmBtn).toBeInTheDocument();
        });
      });

      it("should render the new pet on the list after form submission", async () => {
        await act(async () => {
          const {
            getByLabelText,
            getByTestId,
            getByText,
            container,
          } = renderCreatePetPage();
          await waitForElementToBeRemoved(() => getByTestId("loading"));

          const addNewPetBtn = queryByText(container, /Adicionar outro pet/i);

          fireEvent.click(addNewPetBtn);

          const name = getByLabelText("Nome:");
          fireEvent.change(name, { target: { value: "name2" } });
          const race = getByLabelText("Raça:");
          fireEvent.change(race, { target: { value: "race2" } });
          const observations = getByLabelText("Observações:");
          fireEvent.change(observations, { target: { value: "obs2" } });
          const form = getByTestId("form");

          fireEvent.submit(form);

          await waitForElementToBeRemoved(() => getByText(/Cadastrar/i));

          const addNewPetAgainBtn = queryByText(
            container,
            /Adicionar outro pet/i
          );

          fireEvent.click(addNewPetAgainBtn);

          const name2 = getByLabelText("Nome:");
          expect(name2.value).toBe("");
          const race2 = getByLabelText("Raça:");
          expect(race2.value).toBe("");
          const observations2 = getByLabelText("Observações:");
          expect(observations2.value).toBe("");
        });
      });
    });
  });
});
