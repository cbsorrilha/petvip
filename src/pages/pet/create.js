import React, { useEffect, useState, useContext } from "react";
import { useParams, useNavigate } from "@reach/router";
import { getCustomer } from "../../services/customer";
import { createPet, getPets } from "../../services/pet";
import Page from "../../components/page";
import Title from "../../components/title";
import MainButton from "../../components/main-button";
import LoadingIndicator from "../../components/loading-indicator";
import { useForm } from "../../utils/form";
import {
  disclaimerStyles,
  customerNameStyles,
  containerStyles,
  formStyles,
  controlBarStyles,
  petInfoStyles,
  petNameStyles,
  petRaceStyles,
  petObsStyles,
  petListStyles,
  petListTrack,
} from "./create.module.css";
import PetshopContext from "../../contexts/petshop";
import { toast } from "react-toastify";

export default function CreatePet() {
  const navigate = useNavigate();
  const { customerId } = useParams();

  const cleanForm = {
    name: "",
    race: "",
    observation: "",
    ownerId: customerId,
  };

  const [form, updateField, setForm] = useForm(cleanForm);
  const { petshop } = useContext(PetshopContext);
  const [isLoading, setIsLoading] = useState(true);
  const [isSending, setIsSending] = useState(false);
  const [isAdding, setIsAdding] = useState(false);
  const [customer, setCustomer] = useState({});
  const [pets, setPets] = useState([]);
  const hasPets = pets.length > 0;

  useEffect(() => {
    setIsLoading(true);
    Promise.all([getCustomer(customerId), getPets(customerId)])
      .then(([customer, pets]) => {
        setIsLoading(false);
        setCustomer(customer);
        setPets(pets);
      })
      .catch((error) => {
        if (/Cliente não existe/.test(error.message)) {
          toast.error(error.message, {
            position: toast.POSITION.BOTTOM_CENTER,
          });
          navigate(`/app`);
        }
      });
  }, [customerId, navigate]);

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    setIsSending(true);
    try {
      const pet = await createPet(petshop)({ ...form });
      setIsSending(false);
      setIsAdding(false);
      setPets([...pets, pet]);
      setForm(cleanForm);
    } catch (error) {
      setIsSending(false);
      console.error(error);
      toast.error("Não foi possível cadastrar o pet!", {
        position: toast.POSITION.BOTTOM_CENTER,
      });
    }
  };

  const confirmPets = (e) => {
    e.preventDefault();
    navigate(`/app`);
  };

  if (isLoading) {
    return <LoadingIndicator />;
  }

  return (
    <Page isStart>
      <Title icon="fa-paw">Novos Pets</Title>
      <div className={containerStyles}>
        <p className={disclaimerStyles}>Cadastre os pets de</p>
        <strong className={customerNameStyles}>{customer.name}</strong>
        {hasPets && (
          <div className="has-text-right">
            {!isAdding && (
              <button
                className="has-text-primary button is-text"
                onClick={() => setIsAdding(true)}
              >
                <i className="fa fa-plus" />
                <strong>Adicionar outro pet</strong>
              </button>
            )}
            {isAdding && (
              <button
                className="has-text-grey-light button is-text"
                onClick={() => setIsAdding(false)}
              >
                <strong>Cancelar</strong>
              </button>
            )}
          </div>
        )}
        {(!hasPets || isAdding) && (
          <form
            className={formStyles}
            action="#"
            data-testid="form"
            onSubmit={handleFormSubmit}
          >
            <div className={`field`}>
              <label htmlFor="name" className={`label`}>
                Nome:
              </label>
              <div className="control">
                <input
                  type="text"
                  name="name"
                  id="name"
                  value={form.name}
                  onChange={updateField}
                  required
                  className={`input is-rounded`}
                  placeholder="Bolinha"
                />
              </div>
            </div>
            <div className={`field`}>
              <label htmlFor="race" className={`label`}>
                Raça:
              </label>
              <div className="control">
                <input
                  type="text"
                  name="race"
                  id="race"
                  value={form.race}
                  onChange={updateField}
                  required
                  className={`input is-rounded`}
                  placeholder="Pinscher"
                />
              </div>
            </div>
            <div className={`field`}>
              <label htmlFor="observation" className={`label`}>
                Observações:
              </label>
              <div className="control">
                <textarea
                  name="observation"
                  id="observation"
                  value={form.observation}
                  onChange={updateField}
                  rows="2"
                  className={`textarea is-rounded`}
                />
              </div>
            </div>
            <MainButton isLoading={isSending}>Cadastrar</MainButton>
          </form>
        )}
        {hasPets && !isAdding && (
          <form action="#" onSubmit={confirmPets} className={petListStyles}>
            <div className={petListTrack}>
              {pets.map((pet, index) => (
                <div key={`pet${index}`}>
                  <div className={controlBarStyles}>
                    <button
                      type="button"
                      className="has-text-grey is-text button"
                    >
                      <i className="fa fa-pen" />
                    </button>
                    <button
                      type="button"
                      className="has-text-grey is-text button"
                    >
                      <i className="fa fa-trash" />
                    </button>
                  </div>
                  <div className={petInfoStyles}>
                    <div className={petNameStyles}>
                      <strong>Nome:</strong> {pet.name}
                    </div>
                    <div className={petRaceStyles}>
                      <strong>Raça:</strong> {pet.race}
                    </div>
                    <div className={petObsStyles}>
                      <strong>Observações:</strong>
                      <div>{pet.observation}</div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
            <MainButton>Confirmar</MainButton>
          </form>
        )}
      </div>
    </Page>
  );
}
