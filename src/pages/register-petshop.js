import React, { useState, useContext } from "react";
import { useNavigate, Redirect } from "@reach/router";
import { toast } from "react-toastify";
import Page from "../components/page";
import {
  title,
  formGrid,
  name,
  zipCode,
  address,
  number,
  compliment,
  neighbourhood,
  city,
  state,
  button,
} from "./register-petshop.module.css";
import states from "../utils/states";
import zipCodeService from "../services/zipcode";
import { createPetshop } from "../services/petshops";
import AuthInstanceContext from "../contexts/authInstance";
import PetshopContext from "../contexts/petshop";
import { useForm } from "../utils/form";

export default function RegisterPetshop() {
  const { user } = useContext(AuthInstanceContext);
  const [isSearchingZipCode, setSearchingZipCode] = useState(false);
  const [isSending, setSending] = useState(false);
  const { petshop, setPetshop } = useContext(PetshopContext);

  const [form, updateField, setForm] = useForm({
    name: "",
    zipCode: "",
    address: "",
    number: "",
    compliment: "",
    neighbourhood: "",
    city: "",
    state: "RJ",
    ownerId: user.uid,
  });

  const navigate = useNavigate();

  const searchZipCode = async (event) => {
    try {
      setSearchingZipCode(true);
      const { data } = await zipCodeService.get(event.target.value);
      const { logradouro, bairro, localidade, uf } = data;
      setForm({
        ...form,
        address: logradouro,
        neighbourhood: bairro,
        city: localidade,
        state: uf,
      });
      setSearchingZipCode(false);
    } catch (error) {
      setSearchingZipCode(false);
    }
  };

  const sendNewPetshop = async (e) => {
    e.preventDefault();
    setSending(true);
    try {
      const res = await createPetshop(form);
      setPetshop(res);
      navigate(`/app`);
    } catch (error) {
      setSending(false);
      toast.error("Não foi possível cadastrar o petshop!", {
        position: toast.POSITION.BOTTOM_CENTER,
      });
    }
  };

  if (petshop) {
    return <Redirect to="/app" noThrow />;
  }

  return (
    <Page>
      <h2 className={`title has-text-centered ${title}`}>Novo Petshop</h2>
      <form
        className={formGrid}
        action="#"
        data-testid="form"
        onSubmit={sendNewPetshop}
      >
        <div className={`field ${name}`}>
          <label htmlFor="name" className={`label`}>
            Nome:
          </label>
          <div className="control">
            <input
              type="text"
              name="name"
              id="name"
              value={form.name}
              onChange={updateField}
              required
              className="input is-rounded"
              placeholder="TOP PET 5000"
            />
          </div>
        </div>

        <div className={`field ${zipCode}`}>
          <label htmlFor="zipCode" className={`label`}>
            CEP:
          </label>
          <div className="control">
            <input
              type="text"
              disabled={isSearchingZipCode}
              name="zipCode"
              id="zipCode"
              value={form.zipCode}
              onChange={updateField}
              required
              onBlur={searchZipCode}
              className={`input is-rounded ${
                isSearchingZipCode ? "is-loading" : ""
              }`}
              placeholder="11111-110"
            />
          </div>
        </div>
        <div className={`field ${address}`}>
          <label htmlFor="address" className={`label`}>
            Endereço:
          </label>
          <div className="control">
            <input
              type="text"
              disabled={isSearchingZipCode}
              name="address"
              id="address"
              value={form.address}
              onChange={updateField}
              required
              className="input is-rounded"
              placeholder="Rua dos pets"
            />
          </div>
        </div>
        <div className={`field ${number}`}>
          <label htmlFor="number" className={`label`}>
            Nº:
          </label>
          <div className="control">
            <input
              type="text"
              disabled={isSearchingZipCode}
              name="number"
              id="number"
              value={form.number}
              onChange={updateField}
              required
              className="input is-rounded"
              placeholder="8000"
            />
          </div>
        </div>
        <div className={`field ${compliment}`}>
          <label htmlFor="compliment" className={`label`}>
            Complemento:
          </label>
          <div className="control">
            <input
              type="text"
              disabled={isSearchingZipCode}
              name="compliment"
              id="compliment"
              value={form.compliment}
              onChange={updateField}
              className="input is-rounded"
              placeholder="apto. 6"
            />
          </div>
        </div>
        <div className={`field ${neighbourhood}`}>
          <label htmlFor="neighbourhood" className={`label`}>
            Bairro:
          </label>
          <div className="control">
            <input
              type="text"
              disabled={isSearchingZipCode}
              name="neighbourhood"
              id="neighbourhood"
              value={form.neighbourhood}
              onChange={updateField}
              required
              className="input is-rounded"
              placeholder="Vila dos Pets"
            />
          </div>
        </div>

        <div className={`field ${city}`}>
          <label htmlFor="city" className={`label`}>
            Cidade:
          </label>
          <div className="control">
            <input
              type="text"
              disabled={isSearchingZipCode}
              name="city"
              id="city"
              value={form.city}
              onChange={updateField}
              required
              className="input is-rounded"
              placeholder="Petlândia"
            />
          </div>
        </div>
        <div className={`field ${state}`}>
          <label htmlFor="state" className={`label`}>
            UF:
          </label>
          <div className="control select is-rounded is-full-width">
            <select
              name="state"
              id="state"
              required
              onChange={updateField}
              value={form.state}
            >
              {states.map(({ isoCode }) => (
                <option key={`${isoCode}key`} value={isoCode}>
                  {isoCode}
                </option>
              ))}
            </select>
          </div>
        </div>
        <button
          className={`button is-medium is-fullwidth is-primary is-rounded ${
            isSending ? "is-loading" : ""
          } ${button}`}
          type="submit"
        >
          Cadastrar
        </button>
      </form>
    </Page>
  );
}
