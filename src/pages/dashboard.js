import React from 'react';
import { Router } from '@reach/router';
import DashboardHome from './dashboard-home';
import Navbar from '../components/navbar';
import CreateCustomer from './customer/create';
import CreatePet from './pet/create';
import ListCustomer from './customer/list';
import EditCustomer from './customer/edit';

export default function Dashboard() {
  return (
    <>
      <Router>
        <DashboardHome path='/' />
        <ListCustomer path='clientes' />
        <EditCustomer path='clientes/:customerId/editar' />
        <CreateCustomer path='clientes/adicionar' />
        <CreatePet path='clientes/:customerId/pets/adicionar' />
      </Router>
      <Navbar />
    </>
  );
}
