import React from "react";
import LoadingIndicator from "../components/loading-indicator";

export default function LoadingPage() {
  return <LoadingIndicator />;
}
