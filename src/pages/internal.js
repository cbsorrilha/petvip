import React, { useState, useEffect } from "react";
import { Router } from "@reach/router";
import AuthCheckHOC from "../utils/auth-check";
import RegisterPetshop from "./register-petshop";
import Dashboard from "./dashboard";
import AuthInstanceContext from "../contexts/authInstance";
import PetshopContext from "../contexts/petshop";
import { getPetshop } from "../services/petshops";
import LoadingIndicator from "../components/loading-indicator";

export default AuthCheckHOC(function Internal({ authInstance, user }) {
  const [petshop, setPetshop] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getPetshop(user).then(data => {
      setPetshop(data);
      setLoading(false);
    });
  }, [user]);

  if (loading) {
    return <LoadingIndicator />;
  }

  return (
    <AuthInstanceContext.Provider value={{ authInstance, user }}>
      <PetshopContext.Provider value={{ petshop, setPetshop }}>
        <Router>
          <Dashboard path="/*" />
          <RegisterPetshop path="register" />
        </Router>
      </PetshopContext.Provider>
    </AuthInstanceContext.Provider>
  );
});
