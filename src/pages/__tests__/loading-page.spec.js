import React from "react";
import { render } from "@testing-library/react";
import LoadingPage from "../loading-page";

describe("Test the loading page", () => {
  it("should render the loading page", async () => {
    const { getByTestId } = render(<LoadingPage />);
    const loadingElement = getByTestId("loading");
    expect(loadingElement).toBeInTheDocument();
  });
});
