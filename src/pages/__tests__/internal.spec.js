import React from "react";
import { act, waitForElementToBeRemoved } from "@testing-library/react";
import Dashboard from "../internal";
import AuthInstanceContext from "../../contexts/authInstance";
import { getPetshop } from "../../services/petshops";
import * as firebase from "firebase/app";
import { renderWithRouter } from "../../test-utils";

function renderDashboard() {
  return renderWithRouter(
    <AuthInstanceContext.Provider
      value={{ user: { uid: "111" }, authInstance: {} }}
    >
      <Dashboard />
    </AuthInstanceContext.Provider>
  );
}

jest.mock("../../services/petshops");

describe("renders the petshop dashboard", () => {
  beforeEach(() => {
    getPetshop.mockResolvedValue(Promise.resolve({ name: "Petshop Test" }));
    firebase.auth = jest.fn().mockReturnValue({
      currentUser: { displayName: "Teste" },
      signOut: () => {
        return true;
      },
      onAuthStateChanged: (func) => {
        func(true);
        return () => {};
      },
    });
  });

  it("should render the page title", async () => {
    await act(async () => {
      const { getByTestId } = renderDashboard();
      const titleElement = getByTestId("loading");
      expect(titleElement).toBeInTheDocument();
    });
  });

  it("should render the page title", async () => {
    await act(async () => {
      const { getByText, getByTestId } = renderDashboard();
      await waitForElementToBeRemoved(() => getByTestId("loading"));
      const titleElement = getByText(/Olá Teste/i);
      expect(titleElement).toBeInTheDocument();
    });
  });
});
