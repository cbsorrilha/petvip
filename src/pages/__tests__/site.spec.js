import React from 'react';
import { render } from '@testing-library/react';
import Site from '../site';

test('renders Petvip - Site title', () => {
  const { getByText } = render(<Site />);
  const titleElement = getByText(/Petvip - App/i);
  expect(titleElement).toBeInTheDocument();
});
