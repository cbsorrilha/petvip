import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import Register from '../register-petshop'
import AuthInstanceContext from '../../contexts/authInstance'
import { createHistory, createMemorySource, LocationProvider,} from '@reach/router'
import { createPetshop } from '../../services/petshops'


function renderWithRouter(
  ui,
  { route = '/', history = createHistory(createMemorySource(route)) } = {}
) {
  return {
    ...render(<LocationProvider history={history}>{ui}</LocationProvider>),
    history,
  }
}


function renderRegister() {
  return renderWithRouter(
    <AuthInstanceContext.Provider value={{user: {uid: '111'}, authInstance: {}}}>
      <Register />
    </AuthInstanceContext.Provider>
  )
}

jest.mock('../../services/petshops')

describe('renders Register Petshop Form', () => {
  beforeEach(() => {
    createPetshop.mockResolvedValue(Promise.resolve())
  })
  it('should render the page title', () => {
    const { getByText } = renderRegister()
    const titleElement = getByText(/Novo Petshop/i)
    expect(titleElement).toBeInTheDocument()
  })

  it('should render the main action button', () => {
    const { getByText } = renderRegister()
    const titleElement = getByText(/Cadastrar/i)
    expect(titleElement).toBeInTheDocument()
  })

  it('should send the form', () => {
    const { getByLabelText, getByTestId } = renderRegister()
    const name = getByLabelText('Nome:')
    fireEvent.change(name, { target: { value: '23' } })
    const zipCode = getByLabelText('CEP:')
    fireEvent.change(zipCode, { target: { value: '23' } })
    const address = getByLabelText('Endereço:')
    fireEvent.change(address, { target: { value: '23' } })
    const number = getByLabelText('Nº:')
    fireEvent.change(number, { target: { value: '23' } })
    const compliment = getByLabelText('Complemento:')
    fireEvent.change(compliment, { target: { value: '23' } })
    const neighbourhood = getByLabelText('Bairro:')
    fireEvent.change(neighbourhood, { target: { value: '23' } })
    const city = getByLabelText('Cidade:')
    fireEvent.change(city, { target: { value: '23' } })
    const state = getByLabelText('UF:')
    fireEvent.change(state, { target: { value: '23' } })
    const form = getByTestId('form')
    fireEvent.submit(form)

    expect(createPetshop).toHaveBeenCalled()
  })
  
})
