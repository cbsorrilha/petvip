import React from 'react';
import MaskedInput from 'react-text-mask';
import Page from '../../components/page';
import Title from '../../components/title';
import MainButton from '../../components/main-button';
import { name, address, telephone, email, formGrid } from './form.module.css';
import { useForm, cellphoneMask } from '../../utils/form';

const emptyCustomer = {
  name: '',
  address: '',
  telephone: '',
  email: '',
};

const defaultTexts = {
  title: 'Novo Cliente',
  mainBtn: 'Cadastrar',
};

export default function CustomerForm({
  customer = emptyCustomer,
  texts = defaultTexts,
  action = 'create',
  formHandler,
  isSending,
}) {
  const [form, updateField] = useForm(customer);

  if (!formHandler) {
    console.error(
      'CustomerForm components need a valid closure function as formHandler prop',
    );
    return null;
  }

  const handleFormSubmit = formHandler(form);

  return (
    <Page isStart>
      <Title icon='fa-user'>{texts.title}</Title>
      <form
        className={formGrid}
        action='#'
        data-testid='form'
        onSubmit={handleFormSubmit}
      >
        <div className={`field ${name}`}>
          <label htmlFor='name' className={`label`}>
            Nome:
          </label>
          <div className='control'>
            <input
              type='text'
              name='name'
              id='name'
              value={form.name}
              onChange={updateField}
              required
              className={`input is-rounded`}
              placeholder='Ubirajara da Silva'
            />
          </div>
        </div>
        <div className={`field ${address}`}>
          <label htmlFor='address' className={`label`}>
            Endereço:
          </label>
          <div className='control'>
            <input
              type='text'
              name='address'
              id='address'
              value={form.address}
              onChange={updateField}
              required
              className={`input is-rounded`}
              placeholder='Rua dos Pets, 95'
            />
          </div>
        </div>
        <div className={`field ${telephone}`}>
          <label htmlFor='telephone' className={`label`}>
            Telefone:
          </label>
          <div className='control'>
            <MaskedInput
              mask={cellphoneMask}
              name='telephone'
              id='telephone'
              value={form.telephone}
              onChange={updateField}
              required
              className={`input is-rounded`}
              placeholder='(00) 00000-0000'
            />
          </div>
        </div>
        <div className={`field ${email}`}>
          <label htmlFor='email' className={`label`}>
            Email:
          </label>
          <div className='control'>
            <input
              type='email'
              name='email'
              id='email'
              value={form.email}
              onChange={updateField}
              required
              className={`input is-rounded`}
              placeholder='bira@bicho.com.br'
            />
          </div>
        </div>
        <MainButton isLoading={isSending}>{texts.mainBtn}</MainButton>
      </form>
    </Page>
  );
}
