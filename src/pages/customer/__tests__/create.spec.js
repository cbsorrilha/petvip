import React from "react";
import { fireEvent } from "@testing-library/react";
import CreateCustomerPage from "../create";
import PetshopContext from "../../../contexts/petshop";
import { renderWithRouter } from "../../../test-utils";
import { createCustomer } from '../../../services/customer'

function renderCreateCustomerPage() {
  return renderWithRouter(
    <PetshopContext.Provider
      value={{ petshop: {ownerId: '1' } }}
    >
      <CreateCustomerPage />
    </PetshopContext.Provider>
  );
}

jest.mock('../../../services/customer')

describe("Test the create customer page", () => {
  beforeEach(() => {
    createCustomer.mockResolvedValue(Promise.resolve())
  })

  it("should render the page title", async () => {
    const { getByText } = renderCreateCustomerPage();
    const loadingElement = getByText(/Novo Cliente/i);
    expect(loadingElement).toBeInTheDocument();
  });

  it('should render the main action button', () => {
    const { getByText } = renderCreateCustomerPage()
    const titleElement = getByText(/Cadastrar/i)
    expect(titleElement).toBeInTheDocument()
  })
  
  it('should send the form', () => {
    const { getByLabelText, getByTestId } = renderCreateCustomerPage()
    const name = getByLabelText('Nome:')
    fireEvent.change(name, { target: { value: '23' } })
    const address = getByLabelText('Endereço:')
    fireEvent.change(address, { target: { value: '23' } })
    const telephone = getByLabelText('Telefone:')
    fireEvent.change(telephone, { target: { value: '23' } })
    const email = getByLabelText('Email:')
    fireEvent.change(email, { target: { value: '23' } })
    const form = getByTestId('form')
    
    fireEvent.submit(form)

    expect(createCustomer).toHaveBeenCalled()
  })
});
