import React from "react";
import {
  fireEvent,
  act,
  waitForElementToBeRemoved,
} from "@testing-library/react";
import ListCustomerPage from "../list";
import PetshopContext from "../../../contexts/petshop";
import { renderWithRouter } from "../../../test-utils";
import { getCustomers } from "../../../services/customer";
import { searchCustomerByQuery } from "../../../services/search";
import { conformToMask } from "react-text-mask";
import { cellphoneMask } from "../../../utils/form";

function renderListCustomersPage() {
  return renderWithRouter(
    <PetshopContext.Provider value={{ petshop: { ownerId: "1" } }}>
      <ListCustomerPage />
    </PetshopContext.Provider>
  );
}

jest.mock("../../../services/customer");
jest.mock("../../../services/search");

describe("Test the customer list page", () => {
  describe("test the customer list without any customers", () => {
    beforeEach(() => {
      getCustomers.mockReturnValue(() =>
        Promise.resolve({ total: 0, data: [] })
      );
    });

    it("should render the page title", async () => {
      try {
        await act(async () => {
          const { getByText } = renderListCustomersPage();
          const titleElement = getByText(/Clientes/i);
          expect(titleElement).toBeInTheDocument();
        });
      } catch (error) {
        console.error(error);
      }
    });

    it("should render the search input", async () => {
      try {
        await act(async () => {
          const { getByPlaceholderText } = renderListCustomersPage();
          const titleElement = getByPlaceholderText(
            /Busque por nome, endereço e telefone/i
          );
          expect(titleElement).toBeInTheDocument();
        });
      } catch (error) {
        console.error(error);
      }
    });

    it("should render the not found message", async () => {
      try {
        await act(async () => {
          const { getByText, getByTestId } = renderListCustomersPage();
          await waitForElementToBeRemoved(() => getByTestId("loading"));
          const titleElement = getByText(
            /Ops, parece que você não tem clientes cadastrados./i
          );
          expect(titleElement).toBeInTheDocument();
        });
      } catch (error) {
        console.error(error);
      }
    });
  });

  describe("test the customer list with few customers (< 3)", () => {
    const telephone = "11111111111";
    const id = "1";
    const name = "John Tester";
    const petName = "bolinha";
    const petId = "10";
    const maskedTelephone = conformToMask(telephone, cellphoneMask(telephone))
      .conformedValue;

    beforeEach(() => {
      getCustomers.mockReturnValue(() =>
        Promise.resolve({
          total: 1,
          data: [
            {
              telephone,
              id,
              name,
              pets: [{ name: petName, id: petId }],
            },
          ],
        })
      );
    });

    it("should render the page title", async () => {
      try {
        await act(async () => {
          const { getByText } = renderListCustomersPage();
          const titleElement = getByText(/Clientes/i);
          expect(titleElement).toBeInTheDocument();
        });
      } catch (error) {
        console.error(error);
      }
    });

    it("should render the search input", async () => {
      try {
        await act(async () => {
          const { getByPlaceholderText } = renderListCustomersPage();
          const titleElement = getByPlaceholderText(
            /Busque por nome, endereço e telefone/i
          );
          expect(titleElement).toBeInTheDocument();
        });
      } catch (error) {
        console.error(error);
      }
    });

    it("should render the unique customer element", async () => {
      try {
        await act(async () => {
          const { getByText, getByTestId } = renderListCustomersPage();
          await waitForElementToBeRemoved(() => getByTestId("loading"));
          const nameElement = getByText(`Nome: ${name}`);
          expect(nameElement).toBeInTheDocument();
          const telephoneElement = getByText(`Telefone: ${maskedTelephone}`);
          expect(telephoneElement).toBeInTheDocument();
          const petsLabel = getByText(/Pets:/i);
          expect(petsLabel).toBeInTheDocument();
          const petsValue = getByText(petName);
          expect(petsValue).toBeInTheDocument();
        });
      } catch (error) {
        console.error(error);
      }
    });

    it("should render the unique customer element with many pets (> 1)", async () => {
      const secondPetName = "lobinho";
      const secondPetId = "20";
      getCustomers.mockReturnValue(() =>
        Promise.resolve({
          total: 1,
          data: [
            {
              telephone,
              id,
              name,
              pets: [
                { name: petName, id: petId },
                { name: secondPetName, id: secondPetId },
              ],
            },
          ],
        })
      );

      try {
        await act(async () => {
          const { getByText, getByTestId } = renderListCustomersPage();
          await waitForElementToBeRemoved(() => getByTestId("loading"));
          const nameElement = getByText(`Nome: ${name}`);
          expect(nameElement).toBeInTheDocument();
          const telephoneElement = getByText(`Telefone: ${maskedTelephone}`);
          expect(telephoneElement).toBeInTheDocument();
          const petsLabel = getByText(/Pets:/i);
          expect(petsLabel).toBeInTheDocument();
          const petsValue = getByText(petName);
          expect(petsValue).toBeInTheDocument();
          const secondPetValue = getByText(`, ${secondPetName}`);
          expect(secondPetValue).toBeInTheDocument();
        });
      } catch (error) {
        console.error(error);
      }
    });
  });

  describe("test the customer list with many customers (> 3)", () => {
    const customers = [
      {
        telephone: "11111111111",
        id: "1",
        name: "Alberto",
        pets: [{ name: "bolinha", id: "10" }],
        maskedTelephone: conformToMask(
          "11111111111",
          cellphoneMask("11111111111")
        ).conformedValue,
      },
      {
        telephone: "2222222222",
        id: "2",
        name: "Bernardo",
        pets: [
          { name: "pipoca", id: "20" },
          { name: "spike", id: "30" },
        ],
        maskedTelephone: conformToMask(
          "2222222222",
          cellphoneMask("2222222222")
        ).conformedValue,
      },
      {
        telephone: "33333333333",
        id: "3",
        name: "Caio",
        pets: [{ name: "Bethoven", id: "40" }],
        maskedTelephone: conformToMask(
          "33333333333",
          cellphoneMask("33333333333")
        ).conformedValue,
      },
      {
        telephone: "44444444444",
        id: "4",
        name: "Daniel",
        pets: [
          { name: "Rex", id: "50" },
          { name: "Thor", id: "60" },
          { name: "Fido", id: "70" },
        ],
        maskedTelephone: conformToMask(
          "44444444444",
          cellphoneMask("44444444444")
        ).conformedValue,
      },
    ];
    beforeEach(() => {
      getCustomers.mockReturnValue(() =>
        Promise.resolve({
          total: customers.length,
          data: customers,
        })
      );
    });

    it("should render the page title", async () => {
      try {
        await act(async () => {
          const { getByText } = renderListCustomersPage();
          const titleElement = getByText(/Clientes/i);
          expect(titleElement).toBeInTheDocument();
        });
      } catch (error) {
        console.error(error);
      }
    });

    it("should render the search input", async () => {
      try {
        await act(async () => {
          const { getByPlaceholderText } = renderListCustomersPage();
          const titleElement = getByPlaceholderText(
            /Busque por nome, endereço e telefone/i
          );
          expect(titleElement).toBeInTheDocument();
        });
      } catch (error) {
        console.error(error);
      }
    });

    it("should render the customer elements", async () => {
      try {
        await act(async () => {
          const { getByText, getByTestId } = renderListCustomersPage();
          await waitForElementToBeRemoved(() => getByTestId("loading"));

          customers.map((customer) => {
            const nameElement = getByText(`Nome: ${customer.name}`);
            expect(nameElement).toBeInTheDocument();
            const telephoneElement = getByText(
              `Telefone: ${customer.maskedTelephone}`
            );
            expect(telephoneElement).toBeInTheDocument();
            customer.pets.map((pet, index) => {
              if (index === 0) {
                const petsValue = getByText(pet.name);
                expect(petsValue).toBeInTheDocument();
                return;
              }
              const petsValue = getByText(`, ${pet.name}`);
              expect(petsValue).toBeInTheDocument();
            });
          });
        });
      } catch (error) {
        console.error(error);
      }
    });

    it("should render the pagination component", async () => {
      try {
        await act(async () => {
          const { getByTestId } = renderListCustomersPage();
          await waitForElementToBeRemoved(() => getByTestId("loading"));

          const pagination = getByTestId("pagination");
          expect(pagination).toBeInTheDocument();
        });
      } catch (error) {
        console.error(error);
      }
    });
  });

  describe("test the search input interactions on customer list page", () => {
    const customers = [
      {
        telephone: "11111111111",
        id: "1",
        name: "Alberto",
        pets: [{ name: "bolinha", id: "10" }],
        maskedTelephone: conformToMask(
          "11111111111",
          cellphoneMask("11111111111")
        ).conformedValue,
      },
      {
        telephone: "2222222222",
        id: "2",
        name: "Bernardo",
        pets: [
          { name: "pipoca", id: "20" },
          { name: "spike", id: "30" },
        ],
        maskedTelephone: conformToMask(
          "2222222222",
          cellphoneMask("2222222222")
        ).conformedValue,
      },
      {
        telephone: "33333333333",
        id: "3",
        name: "Caio",
        pets: [{ name: "Bethoven", id: "40" }],
        maskedTelephone: conformToMask(
          "33333333333",
          cellphoneMask("33333333333")
        ).conformedValue,
      },
      {
        telephone: "44444444444",
        id: "4",
        name: "Daniel",
        pets: [
          { name: "Rex", id: "50" },
          { name: "Thor", id: "60" },
          { name: "Fido", id: "70" },
        ],
        maskedTelephone: conformToMask(
          "44444444444",
          cellphoneMask("44444444444")
        ).conformedValue,
      },
    ];

    const searchedCustomers = [
      {
        telephone: "55555555555",
        id: "1",
        name: "Ubirajara",
        pets: [{ name: "Benji", id: "80" }],
        maskedTelephone: conformToMask(
          "55555555555",
          cellphoneMask("55555555555")
        ).conformedValue,
      },
    ];

    beforeEach(() => {
      getCustomers.mockReturnValue(() =>
        Promise.resolve({
          total: customers.length,
          data: customers,
        })
      );

      searchCustomerByQuery.mockReturnValue(() =>
        Promise.resolve({
          total: searchedCustomers.length,
          data: searchedCustomers,
        })
      );
    });

    it("should render only the customers retrieved by the search", async () => {
      try {
        await act(async () => {
          const {
            getByText,
            getByTestId,
            getByPlaceholderText,
          } = renderListCustomersPage();
          await waitForElementToBeRemoved(() => getByTestId("loading"));

          customers.map((customer) => {
            const nameElement = getByText(`Nome: ${customer.name}`);
            expect(nameElement).toBeInTheDocument();
            const telephoneElement = getByText(
              `Telefone: ${customer.maskedTelephone}`
            );
            expect(telephoneElement).toBeInTheDocument();
            customer.pets.map((pet, index) => {
              if (index === 0) {
                const petsValue = getByText(pet.name);
                expect(petsValue).toBeInTheDocument();
                return;
              }
              const petsValue = getByText(`, ${pet.name}`);
              expect(petsValue).toBeInTheDocument();
            });
          });
          await fireEvent.change(
            getByPlaceholderText(/Busque por nome, endereço e telefone/i),
            { target: { value: "bira" } }
          );
          const searchNameElement = getByText(
            `Nome: ${searchedCustomers[0].name}`
          );
          expect(searchNameElement).toBeInTheDocument();
          const searchTelephoneElement = getByText(
            `Telefone: ${searchedCustomers[0].maskedTelephone}`
          );
          expect(searchTelephoneElement).toBeInTheDocument();
          searchedCustomers[0].pets.map((pet, index) => {
            if (index === 0) {
              const petsValue = getByText(pet.name);
              expect(petsValue).toBeInTheDocument();
              return;
            }
            const petsValue = getByText(`, ${pet.name}`);
            expect(petsValue).toBeInTheDocument();
          });
        });
      } catch (error) {
        console.error(error);
      }
    });

    it("should return to previous list of customers when the search input length is 0", async () => {
      try {
        await act(async () => {
          const {
            getByText,
            getByTestId,
            getByPlaceholderText,
          } = renderListCustomersPage();
          await waitForElementToBeRemoved(() => getByTestId("loading"));

          customers.map((customer) => {
            const nameElement = getByText(`Nome: ${customer.name}`);
            expect(nameElement).toBeInTheDocument();
            const telephoneElement = getByText(
              `Telefone: ${customer.maskedTelephone}`
            );
            expect(telephoneElement).toBeInTheDocument();
            customer.pets.map((pet, index) => {
              if (index === 0) {
                const petsValue = getByText(pet.name);
                expect(petsValue).toBeInTheDocument();
                return;
              }
              const petsValue = getByText(`, ${pet.name}`);
              expect(petsValue).toBeInTheDocument();
            });
          });
          await fireEvent.change(
            getByPlaceholderText(/Busque por nome, endereço e telefone/i),
            { target: { value: "bira" } }
          );
          const searchNameElement = getByText(
            `Nome: ${searchedCustomers[0].name}`
          );
          expect(searchNameElement).toBeInTheDocument();
          const searchTelephoneElement = getByText(
            `Telefone: ${searchedCustomers[0].maskedTelephone}`
          );
          expect(searchTelephoneElement).toBeInTheDocument();
          searchedCustomers[0].pets.map((pet, index) => {
            if (index === 0) {
              const petsValue = getByText(pet.name);
              expect(petsValue).toBeInTheDocument();
              return;
            }
            const petsValue = getByText(`, ${pet.name}`);
            expect(petsValue).toBeInTheDocument();
          });

          await fireEvent.change(
            getByPlaceholderText(/Busque por nome, endereço e telefone/i),
            { target: { value: "" } }
          );

          await waitForElementToBeRemoved(() => getByTestId("loading"));

          customers.map((customer) => {
            const nameElement = getByText(`Nome: ${customer.name}`);
            expect(nameElement).toBeInTheDocument();
            const telephoneElement = getByText(
              `Telefone: ${customer.maskedTelephone}`
            );
            expect(telephoneElement).toBeInTheDocument();
            customer.pets.map((pet, index) => {
              if (index === 0) {
                const petsValue = getByText(pet.name);
                expect(petsValue).toBeInTheDocument();
                return;
              }
              const petsValue = getByText(`, ${pet.name}`);
              expect(petsValue).toBeInTheDocument();
            });
          });
        });
      } catch (error) {
        console.error(error);
      }
    });
  });
});
