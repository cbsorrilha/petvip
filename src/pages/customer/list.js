import React, { useContext, useEffect, useReducer } from 'react';
import cc from 'classcat';
import { conformToMask } from 'react-text-mask';
import {
  searchInput,
  searchForm,
  resultsStyle,
  resultStyle,
  noResultsStyle,
  titleClientBox,
  pen,
} from './list.module.css';
import { useForm, cellphoneMask } from '../../utils/form';
import { searchCustomerByQuery } from '../../services/search';
import PetshopContext from '../../contexts/petshop';
import { getCustomers } from '../../services/customer';
import Page from '../../components/page';
import Title from '../../components/title';
import { usePagination } from '../../utils/pagination';
import Pagination from '../../components/pagination';
import LoadingIndicator from '../../components/loading-indicator';
import { navigate } from '@reach/router';

const actionTypes = {
  customerRequest: '@customers/CUSTOMER_REQUEST',
  customerSuccess: '@customers/CUSTOMER_SUCCESS',
  customerFailed: '@customers/CUSTOMER_FAILED',
  customerSearch: '@customers/CUSTOMER_SEARCH',
  customerClearSearch: '@customers/CUSTOMER_CLEAR_SEARCH',
};

const actionCreators = {
  customerRequest: () => ({ type: actionTypes.customerRequest }),
  customerSuccess: (payload) => ({
    type: actionTypes.customerSuccess,
    payload,
  }),
  customerError: () => ({ type: actionTypes.customerFailed }),
  customerSearch: () => ({ type: actionTypes.customerSearch }),
  customerClearSearch: () => ({ type: actionTypes.customerClearSearch }),
};

const initialState = {
  fetching: true,
  total: 0,
  customers: [],
  isSearching: false,
};

const customerListReducer = (state, action) => {
  switch (action.type) {
    case actionTypes.customerRequest:
      return {
        ...state,
        fetching: true,
      };
    case actionTypes.customerSuccess:
      return {
        ...state,
        fetching: false,
        total: action.payload.total,
        customers: action.payload.data,
      };
    case actionTypes.customerFailed:
      return {
        ...state,
        fetching: false,
        total: 0,
        customers: [],
        error: true,
      };
    case actionTypes.customerSearch:
      return {
        ...state,
        fetching: true,
        isSearching: true,
        total: 0,
        customers: [],
      };
    case actionTypes.customerClearSearch:
      return {
        ...state,
        isSearching: false,
      };
    default:
      return state;
  }
};

export default function CustomerList() {
  const [{ fetching, total, customers, isSearching }, dispatch] = useReducer(
    customerListReducer,
    initialState,
  );

  const [pagination, { onPrevious, onNext, onPageSelect }] = usePagination(
    3,
    total,
  );

  const [form, updateField] = useForm({
    query: '',
  });
  const { petshop } = useContext(PetshopContext);

  useEffect(() => {
    if (isSearching) {
      return;
    }
    const fetchData = async () => {
      try {
        dispatch(actionCreators.customerRequest());
        const { total, data } = await getCustomers(petshop)(pagination);
        dispatch(actionCreators.customerSuccess({ total, data }));
      } catch (error) {
        console.error(error);
        dispatch(actionCreators.customerError());
        throw new Error(error);
      }
    };
    fetchData();
  }, [isSearching, pagination, petshop]);

  const handleChange = async (e) => {
    try {
      const { value } = e.target;
      updateField(e);
      if (value.length === 0) {
        dispatch(actionCreators.customerClearSearch());
        dispatch(actionCreators.customerRequest());
        const { offset, limit } = pagination;
        const { total, data } = await getCustomers(petshop)({ offset, limit });
        dispatch(actionCreators.customerSuccess({ total, data }));
        return;
      }
      dispatch(actionCreators.customerSearch());
      if (value.length < 2) {
        return;
      }
      dispatch(actionCreators.customerRequest());
      const { data, total } = await searchCustomerByQuery(petshop.id)({
        q: value,
      });
      dispatch(actionCreators.customerSuccess({ total, data }));
    } catch (error) {
      console.error(error);
      dispatch(actionCreators.customerError());
      throw new Error(error);
    }
  };

  const goToEdit = (customerId) => () => {
    navigate(`clientes/${customerId}/editar`);
  };

  return (
    <Page isStart>
      <Title icon='fa-users'>Clientes</Title>
      <form className={`${searchForm}`}>
        <input
          type='text'
          name='query'
          id='query'
          value={form.query}
          onChange={handleChange}
          className={`input is-rounded ${searchInput}`}
          placeholder='Busque por nome, endereço e telefone'
        />
      </form>
      {fetching && <LoadingIndicator />}
      {(!customers || customers.length < 1) && !fetching && (
        <h3
          className={cc([
            noResultsStyle,
            'has-text-centered',
            'title',
            'is-size-3',
          ])}
        >
          {isSearching
            ? 'A busca não retornou resultados.'
            : 'Ops, parece que você não tem clientes cadastrados.'}
        </h3>
      )}
      {customers && customers.length > 0 && !fetching && (
        <div className={cc([resultsStyle])}>
          {customers.map((result) => {
            const telephoneMask = conformToMask(
              result.telephone,
              cellphoneMask(result.telephone),
            );
            return (
              <div className={cc([resultStyle])} key={result.id}>
                <div className={`${titleClientBox}`}>
                  <div>Nome: {result.name}</div>
                  <div>
                    <button
                      className='has-text-primary button is-text is-size-7'
                      onClick={goToEdit(result.id)}
                    >
                      <i className={`fa fa-pen ${pen}`} />
                      <strong>Editar</strong>
                    </button>
                  </div>
                </div>
                <div className='is-size-7'>
                  Telefone: {telephoneMask.conformedValue}
                </div>
                <div className='is-size-7'>
                  Pets:{' '}
                  {result.pets.map((pet, index) => (
                    <span key={pet.id}>
                      {index !== 0 ? ', ' : ''}
                      {pet.name}
                    </span>
                  ))}
                </div>
              </div>
            );
          })}
        </div>
      )}

      {total > 3 && !fetching && (
        <Pagination
          canPreviousPage={pagination.canPrevious}
          canNextPage={pagination.canNext}
          gotoPage={onPageSelect}
          nextPage={onNext}
          previousPage={onPrevious}
          pageIndex={pagination.page}
          pageCount={pagination.pageCount}
        />
      )}
    </Page>
  );
}
