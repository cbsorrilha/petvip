import React, { useState, useContext } from "react";
import { useNavigate } from "@reach/router";
import { toast } from "react-toastify";
import { createCustomer } from "../../services/customer";
import PetshopContext from "../../contexts/petshop";
import CustomerForm from "./form";

export default function CreateCustomer() {
  const navigate = useNavigate();
  const [isSending, setSending] = useState(false);
  const { petshop } = useContext(PetshopContext);

  const handleFormSubmit = (form) => async (e) => {
    e.preventDefault();
    setSending(true);
    try {
      const customer = await createCustomer(petshop)({ ...form });
      navigate(`/app/clientes/${customer}/pets/adicionar`);
    } catch (error) {
      setSending(false);
      toast.error("Não foi possível cadastrar o cliente!", {
        position: toast.POSITION.BOTTOM_CENTER,
      });
    }
  };

  return <CustomerForm formHandler={handleFormSubmit} isSending={isSending} />;
}
