import React, { useState, useContext, useEffect } from 'react';
import { toast } from 'react-toastify';
import { updateCustomer, getCustomer } from '../../services/customer';
import PetshopContext from '../../contexts/petshop';
import CustomerForm from './form';
import LoadingIndicator from '../../components/loading-indicator';
import { useNavigate, useParams } from '@reach/router';

const texts = {
  mainBtn: 'Editar',
  title: 'Editar Cliente',
};

export default function EditCustomer() {
  const navigate = useNavigate();
  const { customerId } = useParams();
  const [isSending, setSending] = useState(false);
  const { petshop } = useContext(PetshopContext);
  const [isLoading, setIsLoading] = useState(true);
  const [customer, setCustomer] = useState({});

  useEffect(() => {
    setIsLoading(true);
    getCustomer(customerId)
      .then((customer) => {
        setCustomer(customer);
        setIsLoading(false);
      })
      .catch((error) => {
        if (/Cliente não existe/.test(error.message)) {
          toast.error(error.message, {
            position: toast.POSITION.BOTTOM_CENTER,
          });
          navigate(`/app`);
        }
      });
  }, [customerId, navigate]);

  const handleFormSubmit = (form) => async (e) => {
    e.preventDefault();
    setSending(true);
    try {
      await updateCustomer(petshop)(customerId, { ...form });
      toast.success('Cliente editado com sucesso!', {
        position: toast.POSITION.BOTTOM_CENTER,
      });
      setSending(false);
    } catch (error) {
      setSending(false);
      toast.error('Não foi possível cadastrar o cliente!', {
        position: toast.POSITION.BOTTOM_CENTER,
      });
      setSending(false);
    }
  };

  if (isLoading) {
    return <LoadingIndicator />;
  }

  return (
    <CustomerForm
      customer={customer}
      action='edit'
      texts={texts}
      formHandler={handleFormSubmit}
      isSending={isSending}
    />
  );
}
