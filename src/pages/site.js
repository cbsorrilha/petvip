import React from 'react'
import {Link} from '@reach/router'
import { siteContent } from './site.module.css';
import Page from '../components/page';

export default function site() {
  return (
    <Page>
      <div className={siteContent}>
        <h1 className="title has-text-centered">Petvip - App</h1>
        <button className="button is-medium is-fullwidth is-primary is-rounded">
          <Link className="has-text-white has-text-weight-bold" to="/login">Login</Link>
        </button>
      </div>
    </Page>
  )
}
