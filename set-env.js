const fs = require("fs")

const envs = [
  "REACT_APP_FIREBASE_API_KEY",
  "REACT_APP_FIREBASE_AUTH_DOMAIN",
  "REACT_APP_FIREBASE_DATABASE_URL",
  "REACT_APP_FIREBASE_PROJECT_ID",
  "REACT_APP_FIREBASE_STORAGE_BUCKET",
  "REACT_APP_FIREBASE_MESSAGING_SENDER_ID",
  "REACT_APP_FIREBASE_APP_ID",
  "REACT_APP_FIREBASE_MEASUREMENT_ID",
  "REACT_APP_SEARCH_API",
]

const data = fs.readFileSync(".env.production", "utf-8")
const newValue = envs.reduce((acc, env) => {
  const find = new RegExp(`{{${env}}}`, "g")
  acc = acc.replace(find, process.env[env])
  return acc
}, data)

fs.writeFileSync(".env.production", newValue, "utf-8")

console.log("readFileSync complete")
